export const MONTH_NAMES = [
  'Januar',
  'Februar',
  'Mars',
  'April',
  'Mai',
  'Juni',
  'Juli',
  'August',
  'September',
  'Oktober',
  'November',
  'Desember',
];

// Strings
export const CURRENCY = 'NOK';
export const PERIOD = 'Periode';
export const TITLE = 'Luncheon';
export const EXPORT = 'eksporter til excel';
export const SORT_PER_MONTH = 'Sorter på måned';
export const AMOUNT_OF_PURCHASES = 'Antall kjøp';
export const USERS_THIS_MONTH = 'Brukere denne måned';
export const INCOME_THIS_MONTH = 'Total inntekt fra lunsj';
export const NO_LUNCH = 'Ingen lunsj registrert denne måned';